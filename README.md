# Omnote

## Common
These steps apply to both deployment and development environments.

- As root, add node and npm repo: `curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -`
- As root, install node and other dependencies: `apt install nodejs make gcc g++ postgresql postgresql-contrib nginx`
- Switch back to app user. Generate ssh key: `ssh-keygen -t ed25519 -C "kevin@kevinywu.com"`
	- use default location
	- set passphrase if you want
- Add ssh key to Gitlab: [here](https://gitlab.com/profile/keys)
	- get it here: `cat .ssh/id_ed25519.pub`
- Clone repo: `git clone git@gitlab.com:dejawu/omnote.git`
- Install packages: `npm install`
- Create `secrets.json`
	- use top of `server.js` to see what fields are needed
- Configure postgres: from root, do `sudo -i -u postgres`
	- run `psql` to enter the postgres prompt
	- Create migration user: `create role omnote_mig with password 'PASSWORD';`
	- Allow login: `alter role omnote_mig with login;`
	- Create application user: `create role omnote with password 'PASSWORD';`
	- Allow login: `alter role omnote with login;`
	- Create database: `create database omnote with owner omnote_mig;`
	- Set omnote's default privileges on notes created by omnote_mig: `alter default privileges for user omnote_mig in schema public grant insert,select,update,delete on tables to omnote;`
	- Run migrations: `npx knex migrate:latest`
	- Switch to omnote database: `\c omnote`

## Development
- Follow the steps under "Common".
- Configure editor to apply Prettier rules on save (see package.json for specific Prettier flags)
- Run `npm run watch` for live-building and `npm start`to run the server.

## Deployment

- Spin up DO droplet on Ubuntu 18.04.3 LTS
	- We need to be on 18.04 LTS for compatibility with LE
- - Update DNS on CloudFlare. Do this early because it will take time to propagate
- Change root password, required on first login
- Create "omnote" user: `useradd omnote`
- Set password for omnote: `passwd omnote`
- Create "omnote" home directory: `mkdir /home/omnote/`
- Give ownership on home folder to omnote: `chown omnote:omnote /home/omnote`
- Update packages: `apt update && apt upgrade`
- Edit `/etc/ssh/sshd_config` and set `PermitRootLogin yes` to `no`
- Reboot
- Attempt to log back in as root, confirming that it is not allowed (correct password still returns "Permission denied")
- Log in as `omnote`
- Change shell to bash: `chsh -s /bin/bash`
- Follow the steps under "Common".
- Install letsencrypt: without first touching the nginx config, follow the instructions here: `https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx`.
	- Disable CloudFlare proxying for this to work
	- When running certbot for the first time, use the `--nginx` flag
- Update the nginx config: edit `/etc/nginx/sites-available/default` and update the `location / {}` block in the `server` block for SSL (listening on port 443). Replace the default content, if any, with `proxy_pass http://127.0.0.1:3000;`
-   - At this point, attempting to load the site should show nginx's 502 error
- Restart nginx and enable it.
- Add the service: Copy the `omnote.service` file to `/etc/systemd/system` and enable it: `systemctl enable omnote`
- Make sure the deploy script works: fill in the missing hostname in `deploy.sh`