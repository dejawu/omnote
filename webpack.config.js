const webpack = require("webpack");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const BUILD = path.resolve(__dirname, "static/");
const SRC = path.resolve(__dirname, "src/client/");

let config = {
  entry: [
    path.resolve(SRC, "js/client.js"),
    path.resolve(SRC, "scss/main.scss"),
  ],
  output: {
    path: path.resolve(BUILD),
    filename: "bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.jsx?/,
        include: [path.resolve(SRC, "js/"), path.resolve(SRC, "../common/")],
        loader: "babel-loader",
      },
      {
        test: /\.scss$/,
        include: path.resolve(SRC, "scss/"),
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader",
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "bundle.css",
      allChunks: true,
    }),
  ],
};

module.exports = config;
