const {
  migration_user,
  migration_pass,
  db_host,
  db_name,
} = require("./secrets");

module.exports = {
  development: {
    client: "postgres",
    connection: {
      host: db_host,
      user: migration_user,
      password: migration_pass,
      database: db_name,
      multipleStatements: true,
    },
  },
};
