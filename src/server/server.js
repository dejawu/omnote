// local imports
const requireFields = require("./lib/requireFields");

// app config
const SECRETS = require("../../secrets.json");
const missingField = requireFields(
  [
    "env", // Environment setting: DEV, or PRO (use "PRO" for staging environment)
    "domain", // root domain of app
    "port", // port on which to run HTTP server
    "db_host",
    "db_user",
    "db_pass",
    "db_name",
    "migration_user",
    "migration_pass",
  ],
  SECRETS,
);
if (missingField) {
  console.log("Missing required config field: " + missingField);
  process.exit(0);
}

// Express and related
const app = require("express")();
const server = require("http").Server(app);
const path = require("path");
const bodyparser = require("body-parser");

// database and related
const session = require("express-session");
const knex = require("knex")({
  client: "pg",
  connection: {
    host: SECRETS.db_host,
    user: SECRETS.db_user,
    password: SECRETS.db_pass,
    database: SECRETS.db_name,
  },
});

const KnexSessionStore = require("connect-session-knex")(session);
const sessionStore = new KnexSessionStore({
  knex: knex,
});

// logging and instrumentation
const util = require("util");
const { createLogger, format, transports } = require("winston");
const log = createLogger({
  level: SECRETS.env === "DEV" ? "debug" : "info",
  format: format.combine(
    format.timestamp(),
    format.colorize(),
    format.errors({ stack: true }),
    format.printf(info =>
      `${info.timestamp} ${info.level}: ${info.message}\n${Object.entries(info)
        .filter(
          ([key]) =>
            key !== "level" && key !== "message" && key !== "timestamp",
        )
        .reduce(
          (acc, [key, val]) =>
            `${acc}${key}: ${util.inspect(val, { colors: true })}\n`,
          "",
        )}\n`.slice(0, -1),
    ),
  ),
  transports: [new transports.Console()],
});

const cryptoRandomString = require("crypto-random-string");

// express bindings
app.use(bodyparser.json());
app.use(
  session({
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 72 * 60 * 60 * 1000,
    },
    secret:
      SECRETS.env === "DEV"
        ? "dev" // key reused in dev mode for easier debugging
        : cryptoRandomString({ length: 256 }),
  }),
);

app.get("/", (req, res) => {
  res.sendFile(path.join(process.cwd(), "static/index.html"));
});

app.get("/bundle.js", (req, res) => {
  res.sendFile(path.join(process.cwd(), "static/bundle.js"));
});

app.get("/bundle.css", (req, res) => {
  res.sendFile(path.join(process.cwd(), "static/bundle.css"));
});

app.use("/auth", require("./routes/auth")({ knex, log }));
app.use("/api/v1", require("./routes/api")({ knex, log }));

server.listen(SECRETS.port, () => {
  log.info("Listening on " + SECRETS.port);
});
