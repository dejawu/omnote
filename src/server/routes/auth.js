const { env: ENV } = require("../../../secrets.json");
const { jsonSuccessOut, jsonErrorOut } = require("../lib/jsonOut");

const express = require("express");
const auth = express.Router();

module.exports = ({ knex, log }) => {
  const User = require("../actions/User")({
    knex,
    log,
  });

  auth.post("/create", async (request, response) => {
    if (ENV === "PRO") {
      jsonErrorOut(response, 400, "Registration is disabled.");
      return;
    }

    if (!request.body.username || !request.body.password) {
      jsonErrorOut(response, 400, "Expected username and password.");
      return;
    }

    if (request.body.username.length > 20) {
      jsonErrorOut(response, 400, "Username is too long.");
      return;
    }

    // TODO check for all illegal characters

    if (typeof request.body.password !== "string" || !request.body.password) {
      jsonErrorOut(response, 400, "Invalid password.");
      return;
    }

    const username = request.body.username.toLowerCase();

    const [message, code] = await User.create(username, request.body.password);

    if (message) {
      jsonErrorOut(response, code, message);
      return;
    }

    request.session.username = username;

    jsonSuccessOut(response);
  });

  auth.post("/login", async (request, response) => {
    if (!request.body.username || !request.body.password) {
      jsonErrorOut(response, 400, "Expected both username and password.");
      return;
    }

    const [user, message, code] = await User.authenticate(
      request.body.username,
      request.body.password,
    );

    if (message) {
      jsonErrorOut(response, code, message);
      return;
    }

    // good from here, log user in
    request.session.username = request.body.username;

    jsonSuccessOut(response, {
      authed: true,
      username: request.body.username,
    });
  });

  auth.post("/logout", async (request, response) => {
    request.session.destroy();
    jsonSuccessOut(response);
  });

  // returns authentication state and gives the username of the logged-in user if authed
  auth.get("/stat", async (request, response) => {
    // make sure username is still valid, if not, clear it
    if (request.session.username) {
      const [user, message, code] = await User.stat(request.session.username);

      if (message) {
        request.session.destroy();
        request.session.username = undefined;
      }
    }

    if (request.session.username) {
      jsonSuccessOut(response, {
        authed: true,
        username: request.session.username,
      });
    } else {
      jsonSuccessOut(response, {
        authed: false,
      });
    }
  });

  auth.use((request, response, next) => {
    jsonErrorOut(response, 404);
  });

  return auth;
};
