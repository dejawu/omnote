const api = require("express-promise-router")();

const shortid = require("shortid");

const { jsonSuccessOut, jsonErrorOut } = require("../lib/jsonOut");

const requireFields = require("../lib/requireFields");
const pmToHTML = require("../lib/pmToHTML");

const call = require("../../common/call");

module.exports = ({ knex, log }) => {
  const User = require("../actions/User")({ knex, log });
  const Note = require("../actions/Note")({ knex, log });

  // auth-checking middleware
  api.use(async (request, response, next) => {
    if (!request.session) {
      log.warn({
        message: "Request session not set.",
      });
      jsonErrorOut(response, 401);
      return;
    }

    const missingField = requireFields(["username"], request.session);

    if (missingField) {
      log.warn({
        message: "Session was missing field '" + missingField + "', aborting.",
        session: request.session,
      });
      jsonErrorOut(response, 401);
      return;
    }

    let [user, message, code] = await User.stat(request.session.username);

    if (message) {
      log.error({
        message: "Error while validating user session",
        session: request.session,
        error: message,
      });

      jsonErrorOut(response, code, message);
    }

    next();
  });

  // saves note, creating one if necessary
  // returns note id
  api.post("/save", async (request, response) => {
    const { body } = request || {};

    // "version" is the ms timestamp of the document that this edit is based on
    const missing = requireFields(["content", "title"], body);

    if (missing) {
      jsonErrorOut(response, 400, "Missing field: " + missing);
      return;
    }

    if (body.title === "") {
      jsonErrorOut(response, 400, "Cannot save note with empty title.");
      return;
    }

    let noteid;
    let created;
    let updated;

    if (body.noteid) {
      // update existing note
      let [note, message, code] = await Note.stat(
        body.noteid,
        request.session.username,
      );

      if (message) {
        jsonErrorOut(response, code, message);
        return;
      }

      // version checking is optional
      if (body.version && body.version.toString() !== note.updated.toString()) {
        jsonErrorOut(response, 400, "Incorrect version.");
        return;
      }

      created = parseInt(note.created);

      [{ updated = 0 }, message, code] = await Note.update(
        body.noteid,
        body.title,
        body.content,
        request.session.username,
      );

      if (message) {
        jsonErrorOut(response, code, message);
        return;
      }

      noteid = body.noteid;
    } else {
      [{ noteid, created = 0, updated = 0 }, message, code] = await Note.create(
        body.title,
        body.content,
        request.session.username,
      );

      if (message) {
        jsonErrorOut(response, code, message);
        return;
      }
    }

    jsonSuccessOut(response, {
      noteid,
      created,
      updated,
    });
  });

  api.get("/stat", async (request, response) => {
    const { query } = request || {};

    const missing = requireFields(["noteid"], query);

    if (missing) {
      jsonErrorOut(response, 400, "Missing field: " + missing);
      return;
    }

    let [dbResult, error] = await call(
      knex("notes")
        .select("title", "content", "created", "updated")
        .where({
          noteid: query.noteid,
          owner: request.session.username,
        }),
    );

    if (error) {
      log.error({
        message: "Database error while attempting to view note.",
        username: request.session.username,
        error,
      });
      jsonErrorOut(response, 500);
      return;
    }

    if (dbResult.length !== 1) {
      jsonErrorOut(response, 404);
      return;
    }

    // TODO migrate db to jsonb data type
    let content = JSON.parse(dbResult[0].content);
    if (query.html) {
      content = pmToHTML(content);
    }

    jsonSuccessOut(response, {
      title: dbResult[0].title,
      content,
      created: dbResult[0].created,
      updated: dbResult[0].updated,
    });
  });

  api.get("/list", async (request, response) => {
    const { query } = request || {};

    // TODO implement filters

    const limit = query.limit || 30;

    let [dbResult, error] = await call(
      knex("notes")
        .select("noteid", "title", "created", "updated")
        .where({
          owner: request.session.username,
        })
        .orderBy("updated", "desc")
        .limit(limit),
    );

    if (error) {
      log.error({
        message: "Database error while attempting to list user notes.",
        username: request.session.username,
        error,
      });
    }

    jsonSuccessOut(response, {
      results: dbResult,
    });
  });

  api.post("/remove", async (request, response) => {
    const { body } = request || {};

    if (!body.noteid) {
      jsonErrorOut(response, 400, "No note id given.");
      return;
    }

    const [_, message, code] = await Note.remove(
      body.noteid,
      request.session.username,
    );

    if (message) {
      jsonErrorOut(response, code, message);
      return;
    }

    jsonSuccessOut(response);
  });

  // error-catching middleware
  api.use((error, request, response, next) => {
    log.error({
      message: "Unhandled error.",
      username: request.session.username,
      error,
    });

    if (!response.headersSent) {
      jsonErrorOut(response, 500);
    }
  });

  // 404 handler
  api.use((request, response, next) => {
    jsonErrorOut(response, 404);
  });

  return api;
};
