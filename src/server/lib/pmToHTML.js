const { DOMSerializer, Fragment } = require("prosemirror-model");
require("jsdom-global")();
const DOMPurify = require("dompurify")(window);

const schema = require("../../common/schema");

const pmToHTML = content => {
  const fragment = DOMSerializer.fromSchema(schema).serializeFragment(
    Fragment.fromJSON(schema, content),
    {
      document,
    },
  );

  document.body.appendChild(fragment);

  const res = DOMPurify.sanitize(String(document.body.innerHTML));
  document.body.innerHTML = "";
  return res;
};

module.exports = pmToHTML;
