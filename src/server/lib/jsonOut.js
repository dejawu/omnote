const jsonSuccessOut = (response, payload = {}) => {
  response.status(200).json({
    success: true,
    ...payload,
  });
};
const jsonErrorOut = (response, status, message = "", payload = {}) => {
  response.status(status).json({
    success: false,
    ...{
      message:
        message ||
        {
          400: "Invalid request.",
          401: "Unauthorized.",
          403: "Forbidden",
          404: "Resource not found.",
          451: "Unavailable for legal reasons.",
          500: "Internal server error.",
          501: "Not yet implemented.",
          502: "Bad gateway.",
          503: "Service unavailable.",
        }[status],
    },
    ...payload,
  });
};

module.exports = { jsonSuccessOut, jsonErrorOut };
