// Accepts as 'input' an array of strings or an object.
// Returns the first field in list order that is missing, or undefined if all are present.
// Fails silently!
module.exports = (requiredFields, input) => {
  let fieldList;
  if (Array.isArray(input)) {
    fieldList = input;
  } else if (typeof input === "object") {
    fieldList = Object.keys(input);
  } else {
    return;
  }

  return requiredFields.find(field => !fieldList.includes(field));
};
