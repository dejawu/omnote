// actions that apply to users
const bcrypt = require("bcrypt");
const call = require("../../common/call");

module.exports = ({ knex, log = { error: () => {} } }) => {
  // verify user existence (and get metadata)
  const stat = async username => {
    const [result, err] = await call(
      knex("users")
        .select("username")
        .where({
          username,
        }),
    );

    if (err) {
      return [null, "Failed to get user data.", 500];
    }

    if (result.length !== 1) {
      return [null, "User does not exist.", 400];
    }

    return [result[0], null, 200];
  };

  // check password
  // returns [user object or null, error message or null, HTTP code]
  const authenticate = async (username, password) => {
    username = username.toLowerCase();

    const [result, err] = await call(
      knex
        .select("username", "passhash")
        .where({
          username: username,
        })
        .from("users"),
    );

    if (err) {
      log.error({
        message: "Database query for user and passhash failed.",
        error: err,
      });
      return [null, "Error logging in.", 500];
    }

    if (result.length !== 1) {
      // TODO record failed login attempts
      return [null, "Incorrect username or password.", 401];
    }

    const match = await bcrypt.compare(
      password,
      result[0].passhash.toString("utf8"),
    );

    if (!match) {
      return [null, "Incorrect username or password.", 401];
    }

    return [result[0], null, 200];
  };

  // create new user
  // returns [error message or null, appropriate HTTP code]
  const create = async (username, password) => {
    const [_, err] = await call(
      knex("users").insert({
        username,
        passhash: await bcrypt.hash(password, 10),
      }),
    );

    if (username.length === 0 || username.length > 50) {
      return ["Invalid username.", 400];
    }

    if (err) {
      // 23505 is PostgreSQL's code for duplicate keys
      if (err.code === "23505") {
        return ["Username is not available.", 400];
      } else {
        log.error({
          message: "Database error during user creation.",
          error: err,
        });
        return ["Error creating user.", 500];
      }
    }

    return [null, 200];
  };

  // remove user and clean up associated data
  const remove = () => {};

  // list notes owned by user
  const list = () => {};

  return { stat, authenticate, create, remove, list };
};
