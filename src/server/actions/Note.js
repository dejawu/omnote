// actions that apply to individual notes
const shortid = require("shortid");
const call = require("../../common/call");

module.exports = ({ knex, log = { error: () => {} } }) => {
  // verify note exists and get metadata
  const stat = async (noteid, owner = "") => {
    const where = {
      noteid,
    };

    if (owner) {
      where.owner = owner;
    }

    const [result, error] = await call(
      knex("notes")
        .select("noteid", "created", "updated", "owner")
        .where(where),
    );

    if (error) {
      log.error({
        message: "Database error while verifying note before save.",
        owner,
        error,
      });
      return [null, "Error while retrieving note.", 500];
    }

    if (result.length !== 1) {
      log.error({
        message: "Client requested note that does not exist.",
        owner,
      });
      return [null, "Note does not exist.", 404];
    }

    return [result[0], null, 200];
  };

  // create new note
  const create = async (title, content, owner) => {
    // if no note id given, create a new note

    const noteid = shortid.generate();
    const now = Date.now();

    let [result, error] = await call(
      knex("notes").insert({
        noteid,
        owner,
        title,
        content,
        created: now,
        updated: now,
      }),
    );

    if (error) {
      log.error({
        message: "Database error while creating note.",
        owner,
        error,
      });
      return [null, "Error creating note.", 500];
    }

    return [{ noteid, created: now, updated: now }, null, 200];
  };

  // update existing note
  const update = async (noteid, title, content = "", username = "") => {
    const updated = Date.now();

    const where = {
      noteid,
    };

    if (username) {
      where.owner = username;
    }

    if (!title) {
      return [null, "Cannot save note with empty title.", 400];
    }

    // update note
    const [result, error] = await call(
      knex("notes")
        .where(where)
        .update({
          title,
          content,
          updated,
        }),
    );

    if (error) {
      log.error({
        message: "Database error while updating note.",
        username,
        error,
      });
      return [null, "Error occurred while saving note.", 500];
    }

    return [{ updated }, null, 200];
  };

  // delete note and clean up associated data
  const remove = async (noteid, owner = "") => {
    const where = {
      noteid,
    };

    if (owner) {
      where.owner = owner;
    }

    const [result, error] = await call(
      knex("notes")
        .where(where)
        .delete(),
    );

    if (error) {
      log.error({
        message: "Database error while deleting note.",
        error,
      });
      return [null, "Error while deleting note.", 500];
    }

    return [{ noteid }, null, 200];
  };

  return { stat, create, update, remove };
};
