import React, { Component } from "react";
import { render } from "react-dom";

import { view as reactive } from "react-easy-state";

import Header from "./components/Header/Header";
import Editor from "./components/Editor/Editor";
import EditorMenuBar from "./components/Editor/EditorMenuBar";
import List from "./components/List/List";
import Settings from "./components/Settings";

import { env as ENV } from "../../../secrets";

import AppStore from "./stores/App";

class App extends Component {
  constructor(props) {
    super(props);

    window.onhashchange = () => {
      AppStore.setViewFromHash();
    };

    AppStore.init();
  }

  // every view must accept/handle the default param {}
  render() {
    const { view, authed, username } = AppStore;

    const headerComponent = {
      editor: <EditorMenuBar />,
    }[view];

    const viewComponent = {
      home: !!authed ? (
        <div id="home">Welcome back, {username}.</div>
      ) : (
        <div id="home">
          omnote is a prototype note-taking platform, currently in closed alpha.
        </div>
      ),
      editor: <Editor />,
      list: <List />,
      settings: <Settings />,
    }[view];

    return (
      <>
        {ENV === "DEV" && <p id="dev-warn">Dev mode</p>}

        <div id="app" className={"app-" + view}>
          <Header>{headerComponent}</Header>
          {viewComponent}
        </div>
      </>
    );
  }
}

App = reactive(App);

(async () => {
  await AppStore.init();
  render(<App />, document.getElementById("root"));
})();
