import React, { useState } from "react";

import { view as reactive } from "react-easy-state";

import { FiBookOpen, FiEdit, FiUploadCloud, FiTrash2 } from "react-icons/fi";

import formatDistance from "date-fns/formatDistance";

import Tooltip from "../Tooltip";

const ListItem = ({
  hidden,
  title,
  created,
  updated,
  onPreview,
  showPreview,
  previewContent,
  onEdit,
  onPublish,
  onDelete,
}) => {
  const [hovered, setHovered] = useState(-1);

  const buttons = [
    {
      color: "neutral",
      icon: <FiBookOpen />,
      tooltip: "Preview",
      onClick: e => {
        e.preventDefault();
        onPreview();
      },
    },
    {
      color: "neutral",
      icon: <FiEdit />,
      tooltip: "Edit",
      onClick: e => {
        e.preventDefault();
        onEdit();
      },
    },
    {
      color: "neutral",
      icon: <FiUploadCloud />,
      tooltip: "Publish",
    },
    {
      color: "red",
      icon: <FiTrash2 />,
      tooltip: "Delete",
      onClick: e => {
        e.preventDefault();
        onDelete();
      },
    },
  ].map(({ color, icon, tooltip, onClick }, i) => {
    return (
      <div
        onMouseEnter={e => {
          setHovered(i);
        }}
        onMouseLeave={e => {
          setHovered(-1);
        }}
        key={tooltip}
      >
        <a href="#" className={"button-" + color} onClick={onClick}>
          {icon}
        </a>
        <Tooltip visible={hovered === i}>
          <p>{tooltip}</p>
        </Tooltip>
      </div>
    );
  });

  let className = "list-item";
  if (hidden) {
    className += " list-item-hidden";
  }
  if (showPreview) {
    className += " list-item-showPreview";
  }

  return (
    <div className={className}>
      <div className="list-item-meta">
        <div>
          <h3>{title}</h3>
          <div>
            <p className="list-item-created">
              {formatDistance(parseInt(created), Date.now(), {
                addSuffix: true,
              })}
            </p>
            <p className="list-item-updated">
              {formatDistance(parseInt(updated), Date.now(), {
                addSuffix: true,
              })}
            </p>
          </div>
        </div>
        <div className="list-item-buttons">{buttons}</div>
      </div>
      <div
        className="list-item-preview document"
        dangerouslySetInnerHTML={{
          __html: previewContent,
        }}
      />
    </div>
  );
};

export default reactive(ListItem);
