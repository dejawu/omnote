import React, { Component, useState } from "react";

import { view as reactive } from "react-easy-state";

import AppStore from "../../stores/App";
import EditorStore from "../../stores/Editor";

import ListItem from "./ListItem";

import { get, post } from "../../lib/fetch";

class List extends Component {
  // TODO feed in array of notes in props; provide sorting/filtering
  // TODO prop function that handles getting more notes
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      from: 0,
      preview: "",
    };

    this.init();
  }

  init() {
    this.appendItems();
  }

  // deletes ith element in array
  async removeItem(i) {
    const [res, error] = await post("/api/v1/remove", {
      noteid: this.state.items[i].noteid,
    });

    if (error) {
      return;
    }

    this.setState(({ items }) => ({
      items: items.filter((_, j) => j !== i),
    }));
  }

  async appendItems() {
    const [res, error] = await get("/api/v1/list");

    if (error) {
      return;
    }

    this.setState(({ items }) => ({
      items: [...items, ...res.results],
    }));
  }

  async setPreview(noteid) {
    if (this.state.preview && this.state.preview === noteid) {
      this.setState({
        preview: "",
        previewContent: "",
      });
      return;
    }

    const [res, error] = await get("/api/v1/stat", {
      noteid: noteid,
      html: true,
    });

    if (error) {
      return;
    }

    this.setState({
      preview: noteid,
      previewContent: res.content,
    });
  }

  render() {
    const items = this.state.items.map((props, i) => (
      <ListItem
        key={props.noteid}
        hidden={this.state.preview && this.state.preview !== props.noteid}
        onPreview={() => {
          this.setPreview(props.noteid);
        }}
        showPreview={this.state.preview === props.noteid}
        previewContent={
          this.state.preview === props.noteid ? this.state.previewContent : ""
        }
        onEdit={() => {
          AppStore.setView("editor", {
            noteid: props.noteid,
          });
        }}
        onDelete={() => this.removeItem(i)}
        {...props}
      />
    ));

    return <div id="list">{items}</div>;
  }
}

export default reactive(List);
