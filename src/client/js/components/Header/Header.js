import React from "react";

import { view as reactive } from "react-easy-state";

import HeaderLoggedOut from "./HeaderLoggedOut";
import HeaderLoggedIn from "./HeaderLoggedIn";

import AppStore from "../../stores/App";

const Header = ({ children }) => {
  const { authed } = AppStore;

  let headerButtons;
  switch (authed) {
    case undefined:
      headerButtons = null;
      break;
    case true:
      headerButtons = <HeaderLoggedIn />;
      break;
    case false:
      headerButtons = <HeaderLoggedOut />;
  }
  return (
    <div id="header">
      <a
        id="header-logo"
        href="#"
        onClick={e => {
          e.preventDefault();
          AppStore.setView("home");
        }}
      >
        <span>omnote</span>
      </a>
      <div id="header-buttons">{headerButtons}</div>
      {children}
    </div>
  );
};
export default reactive(Header);
