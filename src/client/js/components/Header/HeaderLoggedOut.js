import React, { Component } from "react";

import { FiLogIn, FiUserPlus } from "react-icons/fi";

import { view as reactive } from "react-easy-state";

import AppStore from "../../stores/App";

import Tooltip from "../Tooltip";

import { post } from "../../lib/fetch";

import { env as ENV } from "../../../../../secrets";

class HeaderLoggedOut extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      showLoginForm: false,
      showJoinForm: false,
    };
  }

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleCreateSubmit = async e => {
    e.preventDefault();
    const [res, err] = await post("/auth/create", {
      username: this.state.username,
      password: this.state.password,
    });

    if (err) {
      console.log(err);
      if (err.body) {
        this.setState({ message: err.body.message });
      }
      return;
    }

    AppStore.setAuth(this.state.username);
  };

  handleLoginSubmit = async e => {
    if (e) {
      e.preventDefault();
    }

    // TODO use email to validate
    const [res, err] = await post("/auth/login", {
      username: this.state.username,
      password: this.state.password,
    });

    if (err) {
      if (err.body) {
        this.setState({ message: err.body.message });
      }
      return;
    }

    AppStore.setAuth(this.state.username);
  };

  render() {
    return (
      <>
        <div>
          <a
            href="#"
            className="button-green"
            onClick={e => {
              e.preventDefault();
              this.setState(oldState => ({
                showLoginForm: !oldState.showLoginForm,
                showJoinForm: false,
              }));
            }}
          >
            <FiLogIn />
          </a>
          <Tooltip visible={this.state.showLoginForm}>
            <input
              type="text"
              onChange={this.handleUsernameChange}
              placeholder="Username"
              onKeyPress={e => {
                if (e.key === "Enter") {
                  this.handleLoginSubmit(e);
                }
              }}
              value={this.state.username}
              autoFocus
              name="username"
            />

            <input
              type="password"
              onChange={this.handlePasswordChange}
              placeholder="Password"
              onKeyPress={e => {
                if (e.key === "Enter") {
                  this.handleLoginSubmit(e);
                }
              }}
              value={this.state.password}
              name="password"
            />

            <a
              href="#"
              className="button-green"
              onClick={e => this.handleLoginSubmit(e)}
            >
              Go
            </a>
          </Tooltip>
        </div>

        {ENV === "DEV" && (
          <div>
            <a
              href="#"
              className="button-purple"
              onClick={e => {
                e.preventDefault();
                this.setState(oldState => ({
                  showLoginForm: false,
                  showJoinForm: !oldState.showJoinForm,
                }));
              }}
            >
              <FiUserPlus />
            </a>
            <Tooltip visible={this.state.showJoinForm}>
              <input
                type="text"
                onChange={this.handleUsernameChange}
                placeholder="Username"
                value={this.state.username}
                onKeyPress={e => {
                  if (e.key === "Enter") {
                    this.handleCreateSubmit(e);
                  }
                }}
                autoFocus
              />
              <input
                type="password"
                onChange={this.handlePasswordChange}
                placeholder="Password"
                value={this.state.password}
                onKeyPress={e => {
                  if (e.key === "Enter") {
                    this.handleCreateSubmit(e);
                  }
                }}
              />
              <a
                href="#"
                className="button-purple"
                onClick={e => this.handleCreateSubmit(e)}
              >
                Go
              </a>
            </Tooltip>
          </div>
        )}
      </>
    );
  }
}

export default reactive(HeaderLoggedOut);
