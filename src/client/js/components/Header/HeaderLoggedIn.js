import React, { useState } from "react";

import AppStore from "../../stores/App";

import { view as reactive } from "react-easy-state";

import { FiFilePlus, FiSettings, FiList, FiLogOut } from "react-icons/fi";

import Tooltip from "../Tooltip";

const HeaderLoggedIn = () => {
  const [hovered, setHovered] = useState(-1);

  const buttons = [
    {
      view: "editor",
      color: "neutral",
      icon: <FiFilePlus />,
      tooltip: "New note",
    },
    {
      view: "list",
      color: "neutral",
      icon: <FiList />,
      tooltip: "List, search, filter",
    },
    {
      view: "settings",
      color: "neutral",
      icon: <FiSettings />,
      tooltip: "Settings and utilities",
    },
  ].map(({ view, color, onClick, icon, tooltip }, i) => (
    <div key={tooltip}>
      <a
        href={`#${view}`}
        className={"button-" + color}
        onClick={
          onClick ||
          (e => {
            e.preventDefault();
            AppStore.setView(view);
          })
        }
        onMouseEnter={e => {
          setHovered(i);
        }}
        onMouseLeave={e => {
          setHovered(-1);
        }}
        onTouchEnd={e => {
          setHovered(-1);
        }}
      >
        {icon}
      </a>
      <Tooltip visible={hovered === i}>
        <p>{tooltip}</p>
      </Tooltip>
    </div>
  ));

  return <>{buttons}</>;
};

export default reactive(HeaderLoggedIn);
