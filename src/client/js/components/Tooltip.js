import React, { Component } from "react";

const Tooltip = ({ visible, children }) => (
  <div className="tooltip-container">
    <div className={visible ? "tooltip tooltip-visible" : "tooltip"}>
      {children}
    </div>
  </div>
);

export default Tooltip;
