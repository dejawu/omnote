import React from "react";
import { FiLogOut } from "react-icons/fi";
import { post } from "../lib/fetch";
import Tooltip from "./Tooltip";

import AppStore from "../stores/App";

const Settings = () => (
  <div id="settings">
    <h1>Settings</h1>
    <a
      href="#"
      className="button-red"
      onClick={async e => {
        e.preventDefault();
        const [_, err] = await post("/auth/logout", {});

        if (err) {
          return;
        }

        AppStore.setView("home");
        AppStore.setAuth("");
      }}
    >
      <Tooltip>Log out</Tooltip>
      <FiLogOut />
    </a>
  </div>
);

export default Settings;
