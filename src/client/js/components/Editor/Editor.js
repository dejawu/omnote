import React, { Component } from "react";

import { view as reactive } from "react-easy-state";

import { Fragment } from "prosemirror-model";
import { EditorView } from "prosemirror-view";
import { EditorState } from "prosemirror-state";
import { history } from "prosemirror-history";
import { keymap } from "prosemirror-keymap";
import {
  inputRules,
  textblockTypeInputRule,
  wrappingInputRule,
} from "prosemirror-inputrules";
import markInputRule from "./markInputRule";

import EditorStore from "../../stores/Editor";

import schema from "../../../../common/schema";

import { baseKeymap, menuCommands } from "./commands";

import { get, post } from "../../lib/fetch";

class Editor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
    };

    this.plugins = [
      history(),
      keymap(baseKeymap),
      inputRules({
        // Markdown shortcuts (type Markdown syntax to apply WYSIWYM styles)
        rules: [
          wrappingInputRule(/^\s*>\s$/, schema.nodes.blockquote),
          wrappingInputRule(
            /^(\d+)\.\s$/,
            schema.nodes.ordered_list,
            match => ({ order: +match[1] }),
            (match, node) => node.childCount + node.attrs.order == +match[1],
          ),
          wrappingInputRule(/^\s*([-+*])\s$/, schema.nodes.bullet_list),
          wrappingInputRule(/^\s*```\s$/, schema.nodes.code_block),
          textblockTypeInputRule(
            new RegExp("^(#{1,6})\\s$"),
            schema.nodes.heading,
            match => ({ level: match[1].length }),
          ),
          markInputRule(/(?:\*\*|__)([^*_]+)(?:\*\*|__)$/, schema.marks.strong),
          markInputRule(/(?:^|[^_])(_([^_]+)_)$/, schema.marks.em),
          markInputRule(/(?:^|[^*])(\*([^*]+)\*)$/, schema.marks.em),
          markInputRule(/(?:^|[^*])(\`([^*]+)\`)$/, schema.marks.code),
          markInputRule(
            /\[(\S+)]\(((?:http(s)?:\/\/.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b(?:[-a-zA-Z0-9@:%_+.~#?&/=]*))\)$/,
            schema.marks.link,
          ),
        ],
      }),
    ];

    if (EditorStore.noteid) {
      this.noteid = EditorStore.noteid;
    }

    this.editorTitleRef = React.createRef();
    this.editorState = EditorState.create({
      schema,
      plugins: this.plugins,
    });
  }

  dispatchTransaction = (tr = null, save = tr && tr.docChanged) => {
    if (tr) {
      this.editorState = this.editorState.apply(tr);

      if (save) {
        this.save();
      }
    }
    this.editorView.updateState(this.editorState);
  };

  save() {
    EditorStore.setStatus("save", "Saving...");
    EditorStore.unsaved = true;
    clearTimeout(this.saveTimeoutId);
    this.saveTimeoutId = setTimeout(async () => {
      const body = {
        title: this.state.title,
        content: JSON.stringify(this.editorState.doc.content.toJSON()),
      };

      console.log(this.noteid);

      if (this.noteid) {
        body.noteid = this.noteid;
      }

      if (this.version) {
        body.version = this.version;
      }

      const [res, error] = (await post("/api/v1/save", body)) || [];

      if (error) {
        console.log(error);
        EditorStore.setStatus(
          "err",
          error.status >= 400 && error.status < 500
            ? error.message
            : "Couldn't save note. Please try again later.",
        );
        return;
      }

      if (!this.noteid) {
        EditorStore.noteid = res.noteid;
        this.noteid = res.noteid;
      }

      this.version = res.updated;

      EditorStore.setStatus("ok", "All changes saved.");
      EditorStore.unsaved = false;
    }, 1000);
  }

  createEditorView = async element => {
    this.editorRef = element;

    if (element !== null) {
      this.editorView = new EditorView(element, {
        state: this.editorState,
        scrollMargin: this.editorRef.clientHeight / 2,
        dispatchTransaction: this.dispatchTransaction,
      });

      // focus after the load-in animation is done
      setTimeout(() => {
        this.editorTitleRef.current.focus();
      }, 300);

      // if editor loaded with an existing note, load that note into the contents
      if (EditorStore.noteid) {
        const [res, error] = await get("/api/v1/stat", {
          noteid: EditorStore.noteid,
        });

        if (error) {
          console.log(error);
          return;
        }

        const { content, title, updated } = res;

        const fragment = Fragment.fromJSON(schema, content);

        const tr = this.editorState.tr;
        tr.replaceWith(0, this.editorState.doc.content.size, fragment);
        this.dispatchTransaction(tr, false);

        this.setState({
          title,
          updated,
        });

        this.version = updated;
      }
    }
  };

  render() {
    return (
      <div id="editor" className="document" ref={this.createEditorView}>
        <input
          id="editor-title"
          type="text"
          ref={this.editorTitleRef}
          placeholder="Title"
          onChange={e => {
            this.setState({
              title: e.target.value,
            });
            this.save();
          }}
          onKeyDown={e => {
            if (e.key === "Enter" || e.key === "Tab") {
              e.preventDefault();
              this.editorView.focus();
            }
          }}
          value={this.state.title}
        />
      </div>
    );
  }
}

export default Editor;
