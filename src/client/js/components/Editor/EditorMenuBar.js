import React, { useState } from "react";

import { view as reactive } from "react-easy-state";

import EditorStore from "../../stores/Editor";

import {
  FiCheckCircle,
  FiAlertCircle,
  FiSave,
  FiEdit3,
  FiCloudOff,
  FiDownloadCloud,
} from "react-icons/fi";

import Tooltip from "../Tooltip";

const commandBodies = {
  undo: "↶",
  redo: "↷",
  heading_up: (
    <strong style={{ fontSize: "18px", lineHeight: "22px" }}>
      H<sup>+</sup>
    </strong>
  ),
  heading_down: (
    <strong style={{ fontSize: "18px", lineHeight: "22px" }}>
      H<sup>-</sup>
    </strong>
  ),
  paragraph: "¶",
  bold: <strong>B</strong>,
  italic: <em>I</em>,
  code: <pre style={{ fontSize: 10 }}>&lt;/&gt;</pre>,
  code_block: (
    <pre
      style={{
        lineHeight: 1.0,
        textAlign: "center",
        fontSize: 10,
        margin: "4px 0",
      }}
    >
      &lt;&gt;
      <br />
      &lt;/&gt;
    </pre>
  ),
  blockquote: (
    <blockquote
      style={{
        paddingLeft: 3,
        height: "75%",
      }}
    >
      ❞
    </blockquote>
  ),
};

const commandTitles = {
  undo: "Undo",
  redo: "Redo",
  heading_up: "Larger heading",
  heading_down: "Smaller heading",
  paragraph: "Plain text",
  bold: "Bold",
  italic: "Italic",
  code: "Inline code",
  code_block: "Code block",
  blockquote: "Block quote",
};

const EditorMenuBar = ({ editorView, commands, enabled }) => {
  const [hover, setHover] = useState(false);

  const { status, message, messageVisible } = EditorStore;

  return (
    <>
      {/*<div id="menubar">
        {commands.map(command => (
          <h4
            key={command}
            onClick={e => {
              e.preventDefault();
              if (!enabled[command]) {
                return;
              }

              editorView.focus();
              menuCommands[command](
                editorView.state,
                editorView.dispatch,
                editorView,
              );
            }}
            title={commandTitles[command]}
            className={enabled && enabled[command] ? "" : "disabled"}
          >
            {commandBodies[command]}
          </h4>
        ))}
      </div>*/}
      <div
        id="editor-status"
        className={
          {
            ok: "green",
            err: "red",
            dc: "red",
            // save: "rainbow",
          }[status] || null
        }
      >
        <Tooltip visible={messageVisible || hover}>{message}</Tooltip>
        <div
          onMouseLeave={() => setHover(false)}
          onMouseEnter={() => setHover(true)}
        >
          {
            {
              init: null,
              write: <FiEdit3 size={20} />,
              save: <FiSave size={20} title="Saving..." />,
              ok: <FiCheckCircle size={20} className="green" />,
              err: <FiAlertCircle size={20} className="red" />,
              dc: <FiCloudOff size={20} className="red" title="Disconnected" />,
              load: <FiDownloadCloud size={20} className="red" />,
            }[status]
          }
        </div>
      </div>
    </>
  );
};

export default reactive(EditorMenuBar);
