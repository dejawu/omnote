import React, { useState } from "react";

import Tooltip from "./Tooltip";

const ButtonWithTooltip = ({ children, showOn = "hover" }) => {
  const [visible, setVisible] = useState(false);

  return (
    <a href="#">
      <Tooltip visible={visible}>{children}</Tooltip>
    </a>
  );
};

export default ButtonWithTooltip;
