const get = async (path, body = {}) =>
  await fetch(
    path +
      "/?" +
      Object.entries(body)
        .map(([param, value]) => `${param}=${value}`)
        .join("&"),
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    },
  )
    .then(async r => {
      if (!r.ok) {
        throw {
          status: r.status,
          statusText: r.statusText,
          ...(await r.json()),
        };
      }

      return [await r.json(), null];
    })
    .catch(err => [null, err]);

const post = async (path, body = {}) =>
  await fetch(path, {
    body: JSON.stringify(body),
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "include",
  })
    .then(async r => {
      if (!r.ok) {
        throw {
          status: r.status,
          statusText: r.statusText,
          ...(await r.json()),
        };
      }

      return [await r.json(), null];
    })
    .catch(err => [null, err]);

export { get, post };
