import { store } from "react-easy-state";

let editorMessageTimeout;

const Editor = store({
  _noteid: "",
  set noteid(noteid) {
    if (noteid === "") {
      Editor._noteid = "";
      window.location.hash = `#editor`;
    } else {
      Editor._noteid = noteid;
      window.location.hash = `#editor:${noteid}`;
    }
  },
  get noteid() {
    return Editor._noteid;
  },
  _status: "init",
  _message: "",
  _messageVisible: false,
  get status() {
    return Editor._status;
  },
  get message() {
    return Editor._message;
  },
  get messageVisible() {
    return Editor._messageVisible;
  },
  setStatus: (status, message = "") => {
    Editor._status = status;

    Editor._messageVisible = false;

    if (message) {
      Editor._message = message;
      Editor._messageVisible = true;

      clearTimeout(editorMessageTimeout);
      editorMessageTimeout = setTimeout(() => {
        Editor._messageVisible = false;
      }, 2000);
    }
  },
  _unsaved: false,
  set unsaved(val) {
    Editor._unsaved = val;
    if (val) {
      window.onbeforeunload = () => true;
    } else {
      window.onbeforeunload = undefined;
    }
  },
  get unsaved() {
    return Editor._unsaved;
  },
});

export default Editor;
