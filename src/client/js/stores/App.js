import { store } from "react-easy-state";
import { get } from "../lib/fetch";

import Editor from "./Editor";

const App = store({
  _view: "home",
  get view() {
    // see handleViewChange
    return App._view;
  },
  // replaces handleViewChange
  setView: (view, viewParams = {}) => {
    // disable hashchange events just while we're messing with the hash
    window.onhashchange = undefined;

    switch (view) {
      case "editor":
        const { noteid = "" } = viewParams;

        // if changing notes, clear out note status
        if (noteid !== Editor.noteid) {
          Editor.setStatus("init", "");
        }

        if (noteid) {
          App._view = "editor";
          Editor.noteid = noteid;
          window.location.hash = `#editor:${noteid}`;
        } else {
          App._view = "editor";
          Editor.noteid = "";
          window.location.hash = "#editor";
        }
        break;
      case "list":
        App._view = "list";
        window.location.hash = "#list";
        break;
      case "settings":
        App._view = "settings";
        window.location.hash = "#settings";
        break;
      case "home":
      default:
        App._view = "home";
        Editor.setStatus("init", "");
        window.location.hash = "#home";
    }

    window.onhashchange = () => {
      App.setViewFromHash();
    };
  },
  setViewFromHash: () => {
    const hash = window.location.hash.substring(1);
    let view;
    let viewParams = {};

    if (["list", "share", "settings"].includes(hash)) {
      view = hash;
    } else if (hash.startsWith("editor")) {
      view = "editor";
      viewParams.noteid = hash.includes(":") ? hash.replace("editor:", "") : "";
    } else {
      view = "home";
    }

    App.setView(view, viewParams);
  },
  _username: "",
  _authed: false,
  get username() {
    return App._username;
  },
  get authed() {
    return App._authed;
  },
  setAuth: (username = "") => {
    if (username) {
      App._username = username;
      App._authed = true;
    } else {
      App._username = "";
      App._authed = false;
      App.setView("home");
    }
  },
  init: async () => {
    const [{ authed, username } = {}, err] = await get("/auth/stat");
    if (!authed) {
      App.setView("home");
      App.setAuth("");
      window.location.hash = "";
    } else {
      App.setAuth(username);
      App.setViewFromHash();
    }

    window.onhashchange = () => {
      App.setViewFromHash();
    };
  },
});

export default App;
