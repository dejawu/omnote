// Turns a try-catch into a Go-like error handling pattern
const call = async task => {
  try {
    const result = await task;
    return [result, null];
  } catch (err) {
    return [null, err];
  }
};

module.exports = call;
