exports.up = function(knex) {
  return knex.schema.createTable("published", table => {
    table
      .string("noteid", 14)
      .notNullable()
      .references("notes.noteid")
      .unique();
    table
      .string("owner", 50)
      .notNullable()
      .references("users.username")
      .unique();
    table.text("markup");
    table.string("slug", 128).notNullable();
    table.string("passhash", 60);
    table.boolean("visible");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("published");
};
