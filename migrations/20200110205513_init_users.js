exports.up = function(knex) {
  return knex.schema.createTable("users", table => {
    table
      .string("username", 50)
      .primary()
      .notNullable();
    table.string("passhash", 60).notNullable();
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("users");
};
