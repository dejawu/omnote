exports.up = function(knex) {
  return knex.schema
    .alterTable("published", table => {
      table
        .dropForeign(["noteid"], "published_noteid_foreign")
        .foreign("noteid")
        .references("notes.noteid")
        .onDelete("CASCADE");
      table
        .dropForeign(["owner"], "published_owner_foreign")
        .foreign("owner")
        .references("users.username")
        .onDelete("CASCADE");
    })
    .alterTable("notes", table => {
      table
        .dropForeign(["owner"], "notes_owner_foreign")
        .foreign("owner")
        .references("users.username")
        .onDelete("CASCADE");
    });
};

exports.down = function(knex) {
  console.log("This migration is idempotent, no rollback script is needed");
};
