exports.up = function(knex) {
  return knex.schema.createTable("notes", table => {
    table
      .string("noteid", 14)
      .unique()
      .notNullable();
    table
      .string("owner", 50)
      .notNullable()
      .references("users.username");
    table.string("title").notNullable();
    table.text("content");
    table.bigInteger("created");
    table.bigInteger("updated");
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable("notes");
};
