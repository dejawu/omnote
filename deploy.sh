#!/bin/bash

trap 'exit 1' ERR

if [ "$(whoami)" != "omnote" ]
then
	echo "Not running as app user!"
	exit 1
fi

echo "Fetching dependencies."
npm install

echo "Migrating database."
npx knex migrate:latest

echo "Building."
npm run build

echo "Deployment complete. Run systemctl restart omnote to start the updated server!"

