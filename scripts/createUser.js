const readline = require("readline-promise").default;
const knex = require("./db");

const cryptoRandomString = require("crypto-random-string");

const { create } = require("../src/server/actions/User")({ knex });

const rlp = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
});

(async () => {
  const username = await rlp.questionAsync("Username: ");

  let password = await rlp.questionAsync("Password (blank for random): ");

  if (password === "") {
    const randomPass = cryptoRandomString({ length: 16, type: "base64" });
    password = randomPass;
    console.log("Using password " + randomPass);
  }

  const [message, code] = await create(username, password);

  if (message) {
    console.log(message);
    process.exit(1);
  }

  console.log("User created successfully.");
  process.exit(0);
})();
