const SECRETS = require("../secrets");

module.exports = require("knex")({
  client: "pg",
  connection: {
    host: SECRETS.db_host,
    user: SECRETS.db_user,
    password: SECRETS.db_pass,
    database: SECRETS.db_name,
  },
});
